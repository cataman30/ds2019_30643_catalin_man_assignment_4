﻿using ClientApp.MyService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class ChartsForm : Form
    {
        private int sparetime, toileting, showering, leaving, sleeping, breakfast, grooming, snack, lunch;

        private void button2_Click(object sender, EventArgs e)
        {
            RecommendationForm rf = new RecommendationForm(dateTimePicker1.Value, 1);
            rf.Show();
        }

        public ChartsForm()
        {
            InitializeComponent();
            dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
        }

        private float getHours(int miliseconds)
        {
            return (float)(2.77777778 * (10 / 100000000.0)*miliseconds);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var series in chart1.Series)
            {
                series.Points.Clear();
            }
            
            
            MyService.MyServiceSoapClient client = new MyService.MyServiceSoapClient();
            List<activity> activities = client.getActivities(1);
            Console.WriteLine(activities.ElementAt(1).time_spent);
            sparetime = 0; toileting = 0; showering = 0; leaving = 0; sleeping = 0; breakfast = 0; grooming = 0; snack = 0; lunch = 0;
            foreach (activity a in activities)
            {
                Console.WriteLine(a.start.ToString());

                //Console.WriteLine(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                if ((a.start.HasValue ? a.start.Value.ToString("yyyy-MM-dd") : "NOT AVAILABLE") == dateTimePicker1.Value.ToString("yyyy-MM-dd") )//&& a.end.ToString() == a.start.ToString())
                {
                    switch (a.activity1)
                    {
                        case "Spare_Time/TV":
                            sparetime += (int)a.time_spent;
                            break;
                        case "Toileting":
                            toileting += (int)a.time_spent;
                            break;
                        case "Grooming":
                            grooming += (int)a.time_spent;
                            break;
                        case "Leaving":
                            leaving += (int)a.time_spent;
                            break;
                        case "Breakfast":
                            breakfast += (int)a.time_spent;
                            break;
                        case "Showering":
                            showering += (int)a.time_spent;
                            break;
                        case "Snack":
                            snack += (int)a.time_spent;
                            break;
                        case "Sleeping":
                            sleeping += (int)a.time_spent;
                            break;
                        case "Lunch":
                            lunch += (int)a.time_spent;
                            break;
                    }
                }
            }

            this.chart1.Series["Sleeping"].Points.AddXY("Sleeping", getHours(sleeping));
            this.chart1.Series["Spare_Time/TV"].Points.AddXY("Spare_Time/TV", getHours(sparetime));
            this.chart1.Series["Breakfast"].Points.AddXY("Breakfast", getHours(breakfast));
            this.chart1.Series["Lunch"].Points.AddXY("Lunch", getHours(lunch));
            this.chart1.Series["Snack"].Points.AddXY("Snack", getHours(snack));
            this.chart1.Series["Showering"].Points.AddXY("Showering", getHours(showering));
            this.chart1.Series["Leaving"].Points.AddXY("Leaving", getHours(leaving));
            this.chart1.Series["Grooming"].Points.AddXY("Grooming", getHours(grooming));
            this.chart1.Series["Toileting"].Points.AddXY("Toileting", getHours(toileting));
            this.chart1.DataBind();

            String s = "";

            foreach (activity a in activities)
            {

                if ((a.start.HasValue ? a.start.Value.ToString("yyyy-MM-dd") : "NOT AVAILABLE") == dateTimePicker1.Value.ToString("yyyy-MM-dd"))
                {
                    if (a.activity1 == "Sleep" && getHours((int)a.time_spent) > 12)
                        s += "@" + "The patient slept more than 12 hours!";
                    if (a.activity1 == "Leaving" && getHours((int)a.time_spent) > 12)
                        s += "@" + "The patient was outside for more than 12 hours!";
                    if ((a.activity1 == "Showering" || a.activity1 == "Toileting" || a.activity1 == "Grooming") && getHours((int)a.time_spent) > 1)
                        s += "@" + "The patient spent more than one hour in the bathroom!";
                }
            }

            s = s.Replace("@", System.Environment.NewLine);

            if (s != "")
            {
                System.Windows.Forms.MessageBox.Show(s);
            }
        }
        
    }
}
