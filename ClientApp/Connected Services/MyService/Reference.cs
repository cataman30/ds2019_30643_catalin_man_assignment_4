﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientApp.MyService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfActivity", Namespace="http://tempuri.org/", ItemName="activity")]
    [System.SerializableAttribute()]
    public class ArrayOfActivity : System.Collections.Generic.List<ClientApp.MyService.activity> {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="activity", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class activity : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int idField;
        
        private System.Nullable<int> patient_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string activity1Field;
        
        private System.Nullable<System.DateTime> startField;
        
        private System.Nullable<System.DateTime> endField;
        
        private System.Nullable<int> time_spentField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int id {
            get {
                return this.idField;
            }
            set {
                if ((this.idField.Equals(value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Nullable<int> patient_id {
            get {
                return this.patient_idField;
            }
            set {
                if ((this.patient_idField.Equals(value) != true)) {
                    this.patient_idField = value;
                    this.RaisePropertyChanged("patient_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string activity1 {
            get {
                return this.activity1Field;
            }
            set {
                if ((object.ReferenceEquals(this.activity1Field, value) != true)) {
                    this.activity1Field = value;
                    this.RaisePropertyChanged("activity1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public System.Nullable<System.DateTime> start {
            get {
                return this.startField;
            }
            set {
                if ((this.startField.Equals(value) != true)) {
                    this.startField = value;
                    this.RaisePropertyChanged("start");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public System.Nullable<System.DateTime> end {
            get {
                return this.endField;
            }
            set {
                if ((this.endField.Equals(value) != true)) {
                    this.endField = value;
                    this.RaisePropertyChanged("end");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public System.Nullable<int> time_spent {
            get {
                return this.time_spentField;
            }
            set {
                if ((this.time_spentField.Equals(value) != true)) {
                    this.time_spentField = value;
                    this.RaisePropertyChanged("time_spent");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfTaken_medications", Namespace="http://tempuri.org/", ItemName="taken_medications")]
    [System.SerializableAttribute()]
    public class ArrayOfTaken_medications : System.Collections.Generic.List<ClientApp.MyService.taken_medications> {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="taken_medications", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class taken_medications : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string medication_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string date_takenField;
        
        private System.Nullable<int> patient_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string takenField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int id {
            get {
                return this.idField;
            }
            set {
                if ((this.idField.Equals(value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string medication_name {
            get {
                return this.medication_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.medication_nameField, value) != true)) {
                    this.medication_nameField = value;
                    this.RaisePropertyChanged("medication_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string date_taken {
            get {
                return this.date_takenField;
            }
            set {
                if ((object.ReferenceEquals(this.date_takenField, value) != true)) {
                    this.date_takenField = value;
                    this.RaisePropertyChanged("date_taken");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public System.Nullable<int> patient_id {
            get {
                return this.patient_idField;
            }
            set {
                if ((this.patient_idField.Equals(value) != true)) {
                    this.patient_idField = value;
                    this.RaisePropertyChanged("patient_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string taken {
            get {
                return this.takenField;
            }
            set {
                if ((object.ReferenceEquals(this.takenField, value) != true)) {
                    this.takenField = value;
                    this.RaisePropertyChanged("taken");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MyService.MyServiceSoap")]
    public interface MyServiceSoap {
        
        // CODEGEN: Generating message contract since element name getActivitiesResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getActivities", ReplyAction="*")]
        ClientApp.MyService.getActivitiesResponse getActivities(ClientApp.MyService.getActivitiesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getActivities", ReplyAction="*")]
        System.Threading.Tasks.Task<ClientApp.MyService.getActivitiesResponse> getActivitiesAsync(ClientApp.MyService.getActivitiesRequest request);
        
        // CODEGEN: Generating message contract since element name username from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/loginService", ReplyAction="*")]
        ClientApp.MyService.loginServiceResponse loginService(ClientApp.MyService.loginServiceRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/loginService", ReplyAction="*")]
        System.Threading.Tasks.Task<ClientApp.MyService.loginServiceResponse> loginServiceAsync(ClientApp.MyService.loginServiceRequest request);
        
        // CODEGEN: Generating message contract since element name getMedicationsTakenPerDayResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getMedicationsTakenPerDay", ReplyAction="*")]
        ClientApp.MyService.getMedicationsTakenPerDayResponse getMedicationsTakenPerDay(ClientApp.MyService.getMedicationsTakenPerDayRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getMedicationsTakenPerDay", ReplyAction="*")]
        System.Threading.Tasks.Task<ClientApp.MyService.getMedicationsTakenPerDayResponse> getMedicationsTakenPerDayAsync(ClientApp.MyService.getMedicationsTakenPerDayRequest request);
        
        // CODEGEN: Generating message contract since element name doctorRecommendation from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertRecommendation", ReplyAction="*")]
        ClientApp.MyService.insertRecommendationResponse insertRecommendation(ClientApp.MyService.insertRecommendationRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/insertRecommendation", ReplyAction="*")]
        System.Threading.Tasks.Task<ClientApp.MyService.insertRecommendationResponse> insertRecommendationAsync(ClientApp.MyService.insertRecommendationRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getActivitiesRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getActivities", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.getActivitiesRequestBody Body;
        
        public getActivitiesRequest() {
        }
        
        public getActivitiesRequest(ClientApp.MyService.getActivitiesRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getActivitiesRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idPatient;
        
        public getActivitiesRequestBody() {
        }
        
        public getActivitiesRequestBody(int idPatient) {
            this.idPatient = idPatient;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getActivitiesResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getActivitiesResponse", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.getActivitiesResponseBody Body;
        
        public getActivitiesResponse() {
        }
        
        public getActivitiesResponse(ClientApp.MyService.getActivitiesResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getActivitiesResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ClientApp.MyService.ArrayOfActivity getActivitiesResult;
        
        public getActivitiesResponseBody() {
        }
        
        public getActivitiesResponseBody(ClientApp.MyService.ArrayOfActivity getActivitiesResult) {
            this.getActivitiesResult = getActivitiesResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class loginServiceRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="loginService", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.loginServiceRequestBody Body;
        
        public loginServiceRequest() {
        }
        
        public loginServiceRequest(ClientApp.MyService.loginServiceRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class loginServiceRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string username;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string password;
        
        public loginServiceRequestBody() {
        }
        
        public loginServiceRequestBody(string username, string password) {
            this.username = username;
            this.password = password;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class loginServiceResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="loginServiceResponse", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.loginServiceResponseBody Body;
        
        public loginServiceResponse() {
        }
        
        public loginServiceResponse(ClientApp.MyService.loginServiceResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class loginServiceResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string loginServiceResult;
        
        public loginServiceResponseBody() {
        }
        
        public loginServiceResponseBody(string loginServiceResult) {
            this.loginServiceResult = loginServiceResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getMedicationsTakenPerDayRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getMedicationsTakenPerDay", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.getMedicationsTakenPerDayRequestBody Body;
        
        public getMedicationsTakenPerDayRequest() {
        }
        
        public getMedicationsTakenPerDayRequest(ClientApp.MyService.getMedicationsTakenPerDayRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getMedicationsTakenPerDayRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idPatient;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public System.DateTime date;
        
        public getMedicationsTakenPerDayRequestBody() {
        }
        
        public getMedicationsTakenPerDayRequestBody(int idPatient, System.DateTime date) {
            this.idPatient = idPatient;
            this.date = date;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getMedicationsTakenPerDayResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getMedicationsTakenPerDayResponse", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.getMedicationsTakenPerDayResponseBody Body;
        
        public getMedicationsTakenPerDayResponse() {
        }
        
        public getMedicationsTakenPerDayResponse(ClientApp.MyService.getMedicationsTakenPerDayResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class getMedicationsTakenPerDayResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ClientApp.MyService.ArrayOfTaken_medications getMedicationsTakenPerDayResult;
        
        public getMedicationsTakenPerDayResponseBody() {
        }
        
        public getMedicationsTakenPerDayResponseBody(ClientApp.MyService.ArrayOfTaken_medications getMedicationsTakenPerDayResult) {
            this.getMedicationsTakenPerDayResult = getMedicationsTakenPerDayResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class insertRecommendationRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="insertRecommendation", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.insertRecommendationRequestBody Body;
        
        public insertRecommendationRequest() {
        }
        
        public insertRecommendationRequest(ClientApp.MyService.insertRecommendationRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class insertRecommendationRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int idPatient;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string doctorRecommendation;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public System.DateTime date;
        
        public insertRecommendationRequestBody() {
        }
        
        public insertRecommendationRequestBody(int idPatient, string doctorRecommendation, System.DateTime date) {
            this.idPatient = idPatient;
            this.doctorRecommendation = doctorRecommendation;
            this.date = date;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class insertRecommendationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="insertRecommendationResponse", Namespace="http://tempuri.org/", Order=0)]
        public ClientApp.MyService.insertRecommendationResponseBody Body;
        
        public insertRecommendationResponse() {
        }
        
        public insertRecommendationResponse(ClientApp.MyService.insertRecommendationResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class insertRecommendationResponseBody {
        
        public insertRecommendationResponseBody() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MyServiceSoapChannel : ClientApp.MyService.MyServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MyServiceSoapClient : System.ServiceModel.ClientBase<ClientApp.MyService.MyServiceSoap>, ClientApp.MyService.MyServiceSoap {
        
        public MyServiceSoapClient() {
        }
        
        public MyServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MyServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MyServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MyServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ClientApp.MyService.getActivitiesResponse ClientApp.MyService.MyServiceSoap.getActivities(ClientApp.MyService.getActivitiesRequest request) {
            return base.Channel.getActivities(request);
        }
        
        public ClientApp.MyService.ArrayOfActivity getActivities(int idPatient) {
            ClientApp.MyService.getActivitiesRequest inValue = new ClientApp.MyService.getActivitiesRequest();
            inValue.Body = new ClientApp.MyService.getActivitiesRequestBody();
            inValue.Body.idPatient = idPatient;
            ClientApp.MyService.getActivitiesResponse retVal = ((ClientApp.MyService.MyServiceSoap)(this)).getActivities(inValue);
            return retVal.Body.getActivitiesResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ClientApp.MyService.getActivitiesResponse> ClientApp.MyService.MyServiceSoap.getActivitiesAsync(ClientApp.MyService.getActivitiesRequest request) {
            return base.Channel.getActivitiesAsync(request);
        }
        
        public System.Threading.Tasks.Task<ClientApp.MyService.getActivitiesResponse> getActivitiesAsync(int idPatient) {
            ClientApp.MyService.getActivitiesRequest inValue = new ClientApp.MyService.getActivitiesRequest();
            inValue.Body = new ClientApp.MyService.getActivitiesRequestBody();
            inValue.Body.idPatient = idPatient;
            return ((ClientApp.MyService.MyServiceSoap)(this)).getActivitiesAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ClientApp.MyService.loginServiceResponse ClientApp.MyService.MyServiceSoap.loginService(ClientApp.MyService.loginServiceRequest request) {
            return base.Channel.loginService(request);
        }
        
        public string loginService(string username, string password) {
            ClientApp.MyService.loginServiceRequest inValue = new ClientApp.MyService.loginServiceRequest();
            inValue.Body = new ClientApp.MyService.loginServiceRequestBody();
            inValue.Body.username = username;
            inValue.Body.password = password;
            ClientApp.MyService.loginServiceResponse retVal = ((ClientApp.MyService.MyServiceSoap)(this)).loginService(inValue);
            return retVal.Body.loginServiceResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ClientApp.MyService.loginServiceResponse> ClientApp.MyService.MyServiceSoap.loginServiceAsync(ClientApp.MyService.loginServiceRequest request) {
            return base.Channel.loginServiceAsync(request);
        }
        
        public System.Threading.Tasks.Task<ClientApp.MyService.loginServiceResponse> loginServiceAsync(string username, string password) {
            ClientApp.MyService.loginServiceRequest inValue = new ClientApp.MyService.loginServiceRequest();
            inValue.Body = new ClientApp.MyService.loginServiceRequestBody();
            inValue.Body.username = username;
            inValue.Body.password = password;
            return ((ClientApp.MyService.MyServiceSoap)(this)).loginServiceAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ClientApp.MyService.getMedicationsTakenPerDayResponse ClientApp.MyService.MyServiceSoap.getMedicationsTakenPerDay(ClientApp.MyService.getMedicationsTakenPerDayRequest request) {
            return base.Channel.getMedicationsTakenPerDay(request);
        }
        
        public ClientApp.MyService.ArrayOfTaken_medications getMedicationsTakenPerDay(int idPatient, System.DateTime date) {
            ClientApp.MyService.getMedicationsTakenPerDayRequest inValue = new ClientApp.MyService.getMedicationsTakenPerDayRequest();
            inValue.Body = new ClientApp.MyService.getMedicationsTakenPerDayRequestBody();
            inValue.Body.idPatient = idPatient;
            inValue.Body.date = date;
            ClientApp.MyService.getMedicationsTakenPerDayResponse retVal = ((ClientApp.MyService.MyServiceSoap)(this)).getMedicationsTakenPerDay(inValue);
            return retVal.Body.getMedicationsTakenPerDayResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ClientApp.MyService.getMedicationsTakenPerDayResponse> ClientApp.MyService.MyServiceSoap.getMedicationsTakenPerDayAsync(ClientApp.MyService.getMedicationsTakenPerDayRequest request) {
            return base.Channel.getMedicationsTakenPerDayAsync(request);
        }
        
        public System.Threading.Tasks.Task<ClientApp.MyService.getMedicationsTakenPerDayResponse> getMedicationsTakenPerDayAsync(int idPatient, System.DateTime date) {
            ClientApp.MyService.getMedicationsTakenPerDayRequest inValue = new ClientApp.MyService.getMedicationsTakenPerDayRequest();
            inValue.Body = new ClientApp.MyService.getMedicationsTakenPerDayRequestBody();
            inValue.Body.idPatient = idPatient;
            inValue.Body.date = date;
            return ((ClientApp.MyService.MyServiceSoap)(this)).getMedicationsTakenPerDayAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ClientApp.MyService.insertRecommendationResponse ClientApp.MyService.MyServiceSoap.insertRecommendation(ClientApp.MyService.insertRecommendationRequest request) {
            return base.Channel.insertRecommendation(request);
        }
        
        public void insertRecommendation(int idPatient, string doctorRecommendation, System.DateTime date) {
            ClientApp.MyService.insertRecommendationRequest inValue = new ClientApp.MyService.insertRecommendationRequest();
            inValue.Body = new ClientApp.MyService.insertRecommendationRequestBody();
            inValue.Body.idPatient = idPatient;
            inValue.Body.doctorRecommendation = doctorRecommendation;
            inValue.Body.date = date;
            ClientApp.MyService.insertRecommendationResponse retVal = ((ClientApp.MyService.MyServiceSoap)(this)).insertRecommendation(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ClientApp.MyService.insertRecommendationResponse> ClientApp.MyService.MyServiceSoap.insertRecommendationAsync(ClientApp.MyService.insertRecommendationRequest request) {
            return base.Channel.insertRecommendationAsync(request);
        }
        
        public System.Threading.Tasks.Task<ClientApp.MyService.insertRecommendationResponse> insertRecommendationAsync(int idPatient, string doctorRecommendation, System.DateTime date) {
            ClientApp.MyService.insertRecommendationRequest inValue = new ClientApp.MyService.insertRecommendationRequest();
            inValue.Body = new ClientApp.MyService.insertRecommendationRequestBody();
            inValue.Body.idPatient = idPatient;
            inValue.Body.doctorRecommendation = doctorRecommendation;
            inValue.Body.date = date;
            return ((ClientApp.MyService.MyServiceSoap)(this)).insertRecommendationAsync(inValue);
        }
    }
}
