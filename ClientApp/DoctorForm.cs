﻿using ClientApp.MyService;
using SoapBack.service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class DoctorForm : Form
    {
        public DoctorForm()
        {
            InitializeComponent();
            dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ChartsForm cf = new ChartsForm();
            cf.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MyService.MyServiceSoapClient client = new MyService.MyServiceSoapClient();
            
            DateTime parsedDate = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
            List<taken_medications> tml = client.getMedicationsTakenPerDay(1, parsedDate);
            String s = "";
            foreach(taken_medications tm in tml)
            {
                s = s + "@" + tm.medication_name + "-----" + tm.taken;
            }

            s = s.Replace("@",System.Environment.NewLine);
            System.Windows.Forms.MessageBox.Show(s);
        }
    }
}
