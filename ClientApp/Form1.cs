﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String password = this.textBox2.Text;
            String username = this.textBox1.Text;
            MyService.MyServiceSoapClient client = new MyService.MyServiceSoapClient();
            if(client.loginService(username, password).Contains("doctor"))
            {
                DoctorForm df = new DoctorForm();
                df.Show();
            }
            else
            {
                Console.WriteLine("Login failed!");
            }

        }
    }
}
