﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class RecommendationForm : Form
    {
        DateTime date;
        int patientId;

        public RecommendationForm(DateTime date, int patientId)
        {
            InitializeComponent();
            this.date = date;
            this.patientId = patientId;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyService.MyServiceSoapClient client = new MyService.MyServiceSoapClient();
            client.insertRecommendation(patientId, textBox1.Text, date);
            this.Close();
        }
    }
}
