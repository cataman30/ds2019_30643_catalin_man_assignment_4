﻿using ClientApp.service;
using SoapBack.data;
using SoapBack.db;
using SoapBack.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SoapBack
{
    /// <summary>
    /// Summary description for MyService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MyService : System.Web.Services.WebService
    {

        [WebMethod]
        public List<activity> getActivities(int idPatient)
        {
            ActivityService asvc = new ActivityService();

            return asvc.getActivities(idPatient);
        }


        [WebMethod]
        public String loginService(String username, String password)
        {
            UserService us = new UserService();
            String loginResponse;

            loginResponse = us.logIn(username, password);

            return loginResponse;
        }

        [WebMethod]
        public List<taken_medications> getMedicationsTakenPerDay(int idPatient, DateTime date)
        {
            TakenMedicationsService tms = new TakenMedicationsService();
            return tms.getMedicationsTakenPerDay(idPatient, date);
        }

        [WebMethod]
        public void insertRecommendation(int idPatient, String doctorRecommendation, DateTime date)
        {
            RecommendationService rs = new RecommendationService();
            rs.insertRecommendation(idPatient, doctorRecommendation, date);
        }

        }
    }

