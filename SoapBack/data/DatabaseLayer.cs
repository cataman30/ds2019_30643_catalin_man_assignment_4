﻿using SoapBack.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBack.data
{
    public class DatabaseLayer
    {
        public List<activity> getActivities(int idPatient)
        {
            List<activity> activitiesList = new List<activity>();

            using (Entities db = new Entities())
            {
                var activities = from b in db.activities
                                 select b;

                foreach (activity a in activities)
                {
                    activitiesList.Add(a);
                }
            }
            return activitiesList;
        }

        public List<doctor> getDoctors()
        {
            List<doctor> doctorsList = new List<doctor>();

            using (Entities db = new Entities())
            {
                var doctors = from d in db.doctors
                              select d;
                
                foreach(doctor d in doctors)
                {
                    doctorsList.Add(d);
                }
            }

            return doctorsList;
        }

        public List<user> getUsers()
        {
            List<user> usersList = new List<user>();

            using (Entities db = new Entities())
            {
                var users = from u in db.users
                            select u;

                foreach(user u in users)
                {
                    usersList.Add(u);
                }
            }

            return usersList;
        }

        public List<taken_medications> getTakenMedications()
        {
            List<taken_medications> takenMedicationsList = new List<taken_medications>();

            using (Entities db = new Entities())
            {
                var takenmedications = from tm in db.taken_medications
                            select tm;

                foreach (taken_medications tm in takenmedications)
                {
                    takenMedicationsList.Add(tm);
                }
            }

            return takenMedicationsList;
        }

        public void insertRecommendation(recommendation Recommendation)
        {
            using (Entities db = new Entities())
            {
                db.recommendations.Add(Recommendation);
                db.SaveChanges();
            }
        }

        public List<patient> getPatients()
        {
            List<patient> patientsList = new List<patient>();

            using (Entities db = new Entities())
            {
                var patients = from u in db.patients
                               select u;

                foreach (patient u in patients)
                {
                    patientsList.Add(u);
                }
            }

            return patientsList;
        }
    }
}