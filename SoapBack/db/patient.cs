//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoapBack.db
{
    using System;
    using System.Collections.Generic;
    
    public partial class patient
    {
        public int id { get; set; }
        public string adress { get; set; }
        public Nullable<System.DateTime> birth_date { get; set; }
        public string gender { get; set; }
        public string medical_record { get; set; }
        public string name { get; set; }
        public Nullable<int> id_caregiver { get; set; }
        public Nullable<int> id_user { get; set; }
    
        public virtual caregiver caregiver { get; set; }
        public virtual user user { get; set; }
    }
}
