﻿using SoapBack.data;
using SoapBack.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBack.service
{
    public class ActivityService
    {
        public List<activity> getActivities(int idPatient)
        {
            DatabaseLayer dbl = new DatabaseLayer();

            return dbl.getActivities(idPatient);
        }
    }
}