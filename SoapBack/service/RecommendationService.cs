﻿using SoapBack.data;
using SoapBack.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBack.service
{
    public class RecommendationService
    {
        DatabaseLayer db = new DatabaseLayer();
        
        public recommendation insertRecommendation(int PatientId ,String doctorRecommendation, DateTime dateOfRecommendation)
        {
            recommendation rec = new recommendation();
            
            rec.recommendation1 = doctorRecommendation;
            rec.date = dateOfRecommendation;

            List<patient> patientList = new List<patient>();
            patientList = db.getPatients();

            foreach(patient p in patientList)
            {
                if(p.id == PatientId)
                    rec.caregiver_id = p.id_caregiver;
            }

            db.insertRecommendation(rec);
            return rec;
        }
    }
}