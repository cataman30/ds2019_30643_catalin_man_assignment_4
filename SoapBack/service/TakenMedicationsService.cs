﻿using SoapBack.data;
using SoapBack.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapBack.service
{
    public class TakenMedicationsService
    {
        public List<taken_medications> getMedicationsTakenPerDay(int idPatient, DateTime date)
        {
            DatabaseLayer dbl = new DatabaseLayer();
            List<taken_medications> getAllList = new List<taken_medications>();
            List<taken_medications> filteredList = new List<taken_medications>();
            getAllList = dbl.getTakenMedications();

            foreach (taken_medications tm in getAllList)
            {
                DateTime parsedDate = DateTime.Parse(tm.date_taken);
                int result = DateTime.Compare(date, parsedDate);
                if (result == 0)
                {
                    filteredList.Add(tm);
                    Console.WriteLine("Found RESULT!!!!");
                }
                    
            }

            return dbl.getTakenMedications();
        }
    }
}