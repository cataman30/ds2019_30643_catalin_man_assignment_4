﻿using SoapBack.data;
using SoapBack.db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp.service
{
    class UserService
    {
        public String logIn(String username, String password)
        {
            int userId = -1;
            bool checkUser = false;

            DatabaseLayer dbl = new DatabaseLayer();

            List<user> users = dbl.getUsers();
            List<doctor> doctors = dbl.getDoctors();

            foreach (user u in users)
                if (u.username == username && u.password == password)
                {
                    checkUser = true;
                    userId = u.id;
                    break;
                }

            if (checkUser)
            {
                foreach (doctor d in doctors)
                    if (d.id_user == userId)
                    {
                        return "doctor";
                    }
            }
            return "failedLogIn";
        }
    }
}
